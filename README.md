# auto-heating
A Wemos d1-mini connected to a stepper motor used to adjust the heating level on a radiator. Uses the X113647Stepper (not actually used) library for reference.

![schema](/stepper-motor-arduino.jpg)

## Hardware requirements:
* Wemos D1 Mini (tested with Wavgat D1 mini)
* X113647 Stepper Motor Driver Board
* Stepper motor with 5 pin connector (compatible with X113647 driver board)

## Library deps:
* Stepper (id 874)

## Dev env setup:
0. Clone the repo.
1. Setup a pio env with ```docker pull eclipse/platformio``` and ```docker run -t -i --device=/dev/ttyUSB0 eclipse/platformio bash```
2. Initialize the pio project by running ```platformio init -b wemos``` in the project root.
3. Compile code with ```platformio run```
4. Upload code with ```platformio run -t upload --upload-port /dev/ttyUSB0```

For monitoring the code on the device, use ```platformio serialports monitor``` and choose /dev/ttyUSB0 in the menu.
